import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import background from "../assets/bg.jpg"
import Loading from '../component/Loading'
import Snackbar from '../component/Snackbar'
import Input from '../component/Input'
import Button from '../component/Button'
import { useForm } from "react-hook-form"
import { yupResolver } from "@hookform/resolvers/yup"
import * as yup from "yup"
import Text from '../component/Text'
import logo from "../assets/logo.jpg"
import { createAnggota } from '../store/action/anggota'

function DaftarAnggota() {
    const { loading, msgAngt } = useSelector((state) => state.anggota)
    const [snackBar, setsnackBar] = useState(false)
    const dispatch = useDispatch()
    const method = useForm({
        mode: "all",
        resolver: yupResolver(
            yup.object().shape({
                identitas: yup.string().required("Identitas Tidak Boleh Kosong"),
                nama: yup.string().required("Nama Tidak Boleh Kosong"),
                no_hp: yup.string().required("Nomor Hp Tidak Boleh Kosong"),
                alamat: yup.string().required("Alamat Tidak Boleh Kosong")
            })
        )
    })
    const daftarAnggota = (data) => {
        dispatch(createAnggota(data))
        setsnackBar(true)
        method.reset()
    }
    return (
        <div className='overflow-x-hidden'>
            <Loading show={loading} />
            <Snackbar show={snackBar} toggle={setsnackBar} msg={msgAngt} />
            <img className='absolute w-full h-screen md:blur-0 blur-sm' src={background} alt="" />
            <form className='relative md:w-[30vw] md:h-[100vh] flex flex-col md:justify-center md:ml-10 md:mx-0 mx-3' onSubmit={method.handleSubmit(daftarAnggota)}>
                <div className='md:flex md:space-x-3'>
                    <img className='md:w-12 md:h-12 w-32 h-32 mr-3 rounded-full md:mx-0 mx-[35%] md:mt-0 mt-12' src={logo} alt="" />
                    <Text className="md:text-[3vmin] text-xl mb-5 md:text-left text-center md:mt-0 mt-8 bg-white md:bg-transparent bg-opacity-40 rounded-lg" text="Silahkan Masukkan Data Diri Untuk Menjadi Anggota Nakama. " />
                </div>
                <div className='space-y-5 mb-8'>
                    <Input className="py-2 px-2 w-full" name="No Ktp/Sim" method={method} methodName="identitas" />
                    <Input className="py-2 px-2 w-full" name="Nama" method={method} methodName="nama" />
                    <Input className="py-2 px-2 w-full" name="Nomor Hp" method={method} methodName="no_hp" />
                    <Input className="py-2 px-2 w-full" name="Alamat" method={method} methodName="alamat" />
                </div>
                <Button
                    className="from-sky-600 to-sky-300 py-3 shadow-xl hover:from-sky-700 hover:to-sky-400 bg-gradient-to-bl"
                    name="Daftar"
                    type="submit"
                />
            </form>

        </div>)
}

export default DaftarAnggota