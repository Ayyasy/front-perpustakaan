import React, { useState } from 'react'
import logo from "../assets/logo.jpg"
import { GoListUnordered, GoKebabHorizontal } from "react-icons/go"
import { FiUsers, FiBook, FiLayers } from "react-icons/fi"
import { Link, Outlet, useNavigate } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { IoIosPaperPlane, IoIosPerson } from "react-icons/io"
import { ImExit } from "react-icons/im"
import Dialog from '../component/Dialog'
import { logout } from "../store/action/user"

function HomeAdmin() {
  const [sidebar, setSidebar] = useState(false)
  const nama = useSelector((state) => state.user.username)
  const [profile, setProfile] = useState(false)
  const [dialog, setDialog] = useState(false)
  const dispatch = useDispatch()
  // const { loading } = useSelector((state) => state.user)
  const isLogout = async () => {
    dispatch(logout())
  }
  return (
    <div className={`overflow-x-hidden bg-slate-200`}>
      <Dialog show={dialog} toggle={setDialog}>
        <div className='md:m-10 m-6'>
          <div>
            <h1 className='text-center md:text-4xl text-4xl font-light'>Profile</h1>
          </div>
          <form className='border border-slate-400 md:mt-8 mt-9  flex flex-row justify-evenly'>
            <div className='border-r border-r-slate-400 h-[50vh] w-[18vw] md:px-5 py-4 px-4'>
              <div className='bg-slate-400 pr-10 pl-2 py-2 rounded-lg text-xl mb-3'>
                <h1>Informasi</h1>
              </div>
              <div className='bg-slate-400 pr-10 pl-2 py-2 rounded-lg text-xl'>
                <h1>Ganti Password</h1>
              </div>
            </div>
            <div className='md:px-5 py-4 px-4'>
              <div>
                <h1>{nama}</h1>
              </div>
            </div>
          </form>
        </div>
      </Dialog>
      <div className={`fixed top-0 bg-slate-100 transition-all ease-in-out duration-300 h-screen md:z-0 z-30 md:w-[7vw] w-[70vw] py-2 ${sidebar ? "left-0 " : "-left-[70vw]"} md:left-0`} >
        <div className='mx-1 flex'>
          <img className='w-[70px] h-[70px] rounded-full md:mx-auto' src={logo} alt="" />
          <h1 className={`ml-2 my-auto text-sm md:hidden ${sidebar ? "block" : "hidden"}`}>Perpustakaan Ruang Renung</h1>
        </div>
        <div className={`mx-3 my-7 space-y-2`}>
          <Link to={"/home/daftar-buku"} onClick={() => setSidebar(false)} className={`flex hover:bg-slate-300 md:py-0 py-3 rounded-xl`}>
            <FiBook className={` rounded-lg md:mx-auto mx-2 ${sidebar ? "bg-transparent w-8 h-8" : "active:bg-slate-300 p-2 w-12 h-12"}`} />
            <h1 className={`${sidebar ? "block" : "hidden"} my-auto ml-3`}>Daftar Buku</h1>
          </Link>
          <GoKebabHorizontal className={`w-5 h-5 mx-auto ${sidebar ? "hidden" : "block"}`} />
          <Link to={"/home/daftar-anggota"} onClick={() => setSidebar(false)} className={`flex hover:bg-slate-300 md:py-0 py-3 rounded-xl`}>
            <FiUsers className={` rounded-lg md:mx-auto mx-2 ${sidebar ? "bg-transparent w-8 h-8" : "active:bg-slate-300 p-2 w-12 h-12"}`} />
            <h1 className={`${sidebar ? "block" : "hidden"} my-auto ml-3`}>Daftar Anggota</h1>
          </Link>
          <GoKebabHorizontal className={`w-5 h-5 mx-auto ${sidebar ? "hidden" : "block"}`} />
          <Link to={"/home/daftar-peminjam"} onClick={() => setSidebar(false)} className={`flex hover:bg-slate-300 md:py-0 py-3 rounded-xl`}>
            <FiLayers className={` rounded-lg md:mx-auto mx-2 ${sidebar ? "bg-transparent w-8 h-8" : "active:bg-slate-300 p-2 w-12 h-12"}`} />
            <h1 className={`${sidebar ? "block" : "hidden"} my-auto ml-3`}>Daftar Peminjam</h1>
          </Link>
        </div>
      </div>
      <div className={`bg-slate-200 md:h-full absolute right-0 transition-all ease-in-out duration-300 px-[7px] w-full md:w-[93%]`}>
        <div className={`absolute flex flex-col w-40 right-5 bg-slate-200 transition-all ease-in-out duration-300 shadow-lg rounded-md ${profile ? "visible top-[85px]" : "invisible top-4"}`}>
          <button onClick={() => setDialog(!dialog)} className='py-1 hover:bg-gray-300 px-2 flex text-xl'><IoIosPerson className='my-auto mr-1' />Profile</button>
          <button onClick={isLogout} className='py-1 hover:bg-gray-300 px-2 flex text-xl'><ImExit className='my-auto mr-1' />Logout</button>
        </div>
        <div className='bg-white mt-4 flex justify-between shadow-xl'>
          <GoListUnordered className='md:hidden block w-8 h-8 mx-2 my-auto' onClick={() => setSidebar(true)} />
          <div className='my-auto md:block hidden  px-2'>
            <h1 className='text-xl cursor-default'>Perpustakaan Ruang Renung</h1>
          </div>
          <div className='flex py-2 bg-white z-10 px-2 w-52 justify-between'>
            <h1 className='cursor-default my-auto text-lg ml-7'>{nama}</h1>
            <IoIosPaperPlane className=' p-2 w-12 h-12 rounded-full bg-gradient-to-tr from-slate-400 to-slate-200 cursor-pointer' onClick={() => setProfile(!profile)} />
          </div>
        </div>
        <div className='mt-3 bg-slate-200 h-screen'>
          <Outlet />
        </div>
      </div>
      <div onClick={() => setSidebar(false)} className={sidebar ? `fixed top-0 right-0 w-full h-full bg-black bg-opacity-70 transition-colors ease-in-out duration-500 visible md:hidden block z-10` : `invisible fixed top-0 right-0 w-full h-full bg-black bg-opacity-0 transition-colors ease-in-out duration-500 `}></div>
    </div>
  )
}

export default HomeAdmin