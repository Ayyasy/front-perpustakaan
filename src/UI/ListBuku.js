import React, { useEffect, useState } from 'react'
import Admin from '../api/Admin'
import Table from '../component/Table'
import Loading from '../component/Loading'
import { useDispatch, useSelector } from 'react-redux'
import { getAllBuku,getByKey } from '../store/action/buku'

function ListBuku() {
    // const [loading, setLoading] = useState(false)
    const [key, setKey] = useState("")
    const [textPage, setTextPage] = useState(1)
    const [page, setPage] = useState(1)
    const [row, setRow] = useState(5)
    const dispatch = useDispatch()
    const [totalPage, setTotalPage] = useState([])
    const {allbuku,loading} =  useSelector((state) => state.buku)
    useEffect(() => {
        dispatch(getAllBuku())
        setTotalPage(Math.ceil(allbuku.length / row))
    }, [page,row])
    const cariByKey = () => {
        if (key.trim !== "") {
            dispatch(getByKey(key))
        }
        else {
            dispatch(getAllBuku())
        }
        setPage(1)
        setTextPage(1)
    }

    const nextHandle = () => {
        if (page <= allbuku.length - row) {
            setPage((e) => e += row)
            setTextPage((e) => e += 1)
        }

    }
    const prevHandle = () => {
        if (page > 1) {
            setPage((e) => e -= row)
            setTextPage((e) => e -= 1)
        }
    }
    const changeRow = (e) => {
        setPage(1)
        setTextPage(1)
        if (e.target.value === "all") {
            setRow(allbuku.length)
        }
        else {
            setRow(parseInt(e.target.value))
        }

    }
    return (
        <div>
            <Loading show={loading}/>
            {/* <Dialog show={dialogPinjamBuku} toggle={()=>{
                setBukuOption("")
                setBukuLabel("")
                setDialogPinjamBuku(false)}}>
                <div className='md:m-10 m-6'>
                    <div>
                        <h1 className='text-center md:text-4xl text-4xl font-light'>Tambah Pinjaman</h1>
                    </div>
                    <form onSubmit={methodPinjam.handleSubmit(addPinjaman)} className='border border-slate-400 md:px-8 px-4 md:mt-8 mt-9 py-10 flex flex-col justify-evenly'>
                        <div className='w-full space-y-2'> */}
                            {/* <Input className="py-1 px-2 w-full" name="Nama" method={methodPinjam} methodName="nama" readOnly={true} /> */}
                            {/* <Option method={methodPinjam} methodName="uuid_buku" options={buku} label="buku" onChange={(e) => selectChange(e)} value={{value:bukuOption,label:bukuLabel}}/>
                            <Input className="py-1 px-2 w-full" name="Tanggal Pengembalian" method={methodPinjam} type="date" methodName="tanggalPengembalian" />
                        </div>
                        <button type='submit' className='w-full mt-10 py-3 rounded-lg text-white bg-gradient-to-tr from-slate-600 to-slate-400 hover:from-slate-700 hover:to-slate-500'>Tambah</button>
                    </form>
                </div>
            </Dialog> */}
            <div className='md:p-4 p-2 h-screen bg-slate-300'>
                <Table
                    title={"Daftar Buku"}
                    value={key}
                    onChange={(e) => setKey(e.target.value)}
                    getByKey={cariByKey}
                    titleTable={"List Buku"}
                    searchTitle={"Judul/Pengarang"}
                    textPage={textPage}
                    page={page}
                    prevHandle={prevHandle}
                    nextHandle={nextHandle}
                    row={row}
                    changeRow={changeRow}
                    totalPage={totalPage}
                    classTambah="hidden"

                >
                    <table>
                        <thead className='bg-[#fedcba]'>
                            <tr className='text-lg'>
                                <th className='md:px-5 md:py-3 font-semibold px-5'>No</th>
                                {/* <th className='md:px-5 md:py-3 font-semibold pr-5'>Jumlah</th> */}
                                <th className='font-semibold text-left md:px-2 md:w-[30rem] pr-72'>Judul</th>
                                <th className='font-semibold text-left md:px-2 md:w-[16rem] pl-4 pr-32'>Kategori</th>
                                <th className='font-semibold text-left md:px-2 md:w-[20rem] pl-4 pr-32'>Pengarang</th>
                                <th className='font-semibold text-left md:px-2 md:w-[18rem] pl-4 pr-32'>Iso</th>
                                <th className='font-semibold text-left  md:w-[13rem] pl-4 pr-32'>Pemilik</th>
                            </tr>
                        </thead>
                        <tbody>
                            {allbuku.slice(page - 1, page + (row - 1)).map((data, id) => (
                                <tr key={id} className={`uppercase bg-white hover:bg-slate-200 cursor-pointer`}>
                                    <td className='font-bold text-slate-800 md:px-5 md:py-3 border-b border-b-slate-300 py-5 border-l px-5'>{id + page}</td>
                                    {/* <td className='font-bold text-slate-800 text-left md:px-10 px-4 border-b border-b-slate-300 py-5'>{data.jumlah_buku}</td> */}
                                    <td className='font-bold text-slate-800 text-left md:px-2 border-b border-b-slate-300 py-5'>{data.judul_buku}</td>
                                    <td className='font-bold text-slate-800 text-left md:px-2 border-b border-b-slate-300 py-5 pl-4'>{data.tipe_buku}</td>
                                    <td className='font-bold text-slate-800 text-left md:px-2 border-b border-b-slate-300 py-5 pl-4'>{data.pengarang}</td>
                                    <td className='font-bold text-slate-800 text-left md:px-2 border-b border-b-slate-300 py-5 pl-4'>{data.iso === null || data.iso === "" ? "-" : data.iso}</td>
                                    <td className='font-bold text-slate-800 text-left md:px-2 border-b border-b-slate-300 py-5 pl-4'>{data.pemilik}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </Table>
            </div>
        </div>
    )
}

export default ListBuku