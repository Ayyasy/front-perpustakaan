import { yupResolver } from '@hookform/resolvers/yup';
import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import * as yup from "yup";
import Dialog from '../component/Dialog';
import Loading from '../component/Loading';
import Input from '../component/Input';
import Snackbar from '../component/Snackbar';
import {HiOutlinePencilSquare } from "react-icons/hi2"
import moment from 'moment';
import Table from '../component/Table';
import { useDispatch, useSelector } from 'react-redux';
import { editPeminjamOne, findAllPeminjam, findPeminjamByKey } from '../store/action/peminjam';


function TablePeminjam() {
    const {allpeminjam,loading,msgPinjam} = useSelector((state)=> state.peminjam)
    const [key, setKey] = useState("")
    const [page, setPage] = useState(1)
    const [textPage, setTextPage] = useState(1)
    const [dialog, setDialog] = useState(false)
    const [snack, setsnack] = useState(false)
    const [uuid, setUuid] = useState("")
    const [row, setRow] = useState(5)
    const dispatch = useDispatch()
    const [totalPage, setTotalPage] = useState([])
    const method = useForm({
        mode: "all",
        resolver: yupResolver(
            yup.object().shape({
                tanggalPengembalian: yup.string().required("Tanggal Tidak Boleh Kosong"),
            })
        )
    })
    useEffect(() => {
        dispatch(findAllPeminjam())
        setTotalPage(Math.ceil(allpeminjam.length / row))
    }, [page, row,snack])

    const getByKey = () => {
        if (key.trim !== "") {
            dispatch(findPeminjamByKey(key))
        }
        else {
            dispatch(findAllPeminjam())
        }
        setPage(1)
        setTextPage(1)
    }
    const editPeminjam = async (update) => {
        setDialog(false)
        dispatch(editPeminjamOne({uuid,update}))
        setsnack(true)
    }
    const showEdit = (e) => {
        setDialog(!dialog)
        setUuid(e.uuid)
        method.reset({ nama: e.nama, buku: e.buku_dipinjam, nomorHp: e.no_hp, tanggalPengembalian: e.pinjam_sampai_tgl })

    }
    const nextHandle = () => {
        if (page <= allpeminjam.length - row) {
            setPage((e) => e += row)
            setTextPage((e) => e += 1)
        }

    }
    const prevHandle = () => {
        if (page > 1) {
            setPage((e) => e -= row)
            setTextPage((e) => e -= 1)
        }
    }

    const changeRow = (e) => {
        setPage(1)
        setTextPage(1)
        if (e.target.value === "all") {
            setRow(allpeminjam.length)
        }
        else {
            setRow(parseInt(e.target.value))
        }

    }
    return (
        <div className='overflow-x-hidden'>
            <Loading show={loading} />
            <Snackbar show={snack} toggle={setsnack} msg={msgPinjam} />
            <Dialog show={dialog} toggle={() => {
                method.reset()
                setDialog(false)
            }}>
                <div className='md:m-10 m-6'>
                    <div>
                        <h1 className='text-center md:text-4xl text-4xl font-light'>Edit Peminjam</h1>
                    </div>
                    <form onSubmit={method.handleSubmit(editPeminjam)} className='border border-slate-400 md:px-8 px-4 md:mt-8 mt-9 py-10 flex flex-col justify-evenly'>
                        <div className='w-full space-y-2'>
                            <Input className="py-1 px-2 w-full" name="Tanggal Pengembalian" type="date" method={method} methodName="tanggalPengembalian" />
                        </div>
                        <button type='submit' className='w-full mt-10 py-3 rounded-lg text-white bg-gradient-to-tr from-slate-600 to-slate-400 hover:from-slate-700 hover:to-slate-500'>Edit</button>
                    </form>
                </div>
            </Dialog>
            <Table
                title={"Daftar Peminjam"}
                value={key}
                onChange={(e) => setKey(e.target.value)}
                getByKey={getByKey}
                titleTable={"List Peminjam"}
                searchTitle={"Nama/identitas"}
                textPage={textPage}
                prevHandle={prevHandle}
                nextHandle={nextHandle}
                classTambah="hidden"
                row={row}
                changeRow={changeRow}
                totalPage={totalPage}
            >
                <table>
                    <thead className='bg-[#fedcba]'>
                        <tr className='text-lg'>
                            <th className='md:px-5 md:py-3 font-semibold px-6'>No</th>
                            <th className='font-semibold text-left md:px-2 md:w-[30rem] pr-56'>Nama</th>
                            <th className='font-semibold text-left md:px-2 md:w-[45rem] pr-56 '>Buku</th>
                            <th className='font-semibold text-left md:px-2 md:w-[15rem] pr-32'>nomor Hp</th>
                            <th className='font-semibold text-left md:px-2 md:w-[18rem] pr-20'>tanggal Peminjaman</th>
                            <th className='font-semibold text-left md:px-2 md:w-[18rem] pr-20'>tanggal Pengembalian</th>
                            <th className='font-semibold text-left md:px-2 md:w-[6rem] pr-10'>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                        {allpeminjam !== null || allpeminjam !== undefined ? allpeminjam.slice(page - 1, page + (row - 1)).map((data, id) => (
                            <tr key={id} className={`uppercase`}>
                                <td className='font-bold text-slate-800 md:px-5 md:py-3 border-b border-b-slate-300 border-l pl-7'>{id + page}</td>
                                <td className='font-bold text-slate-800 text-left md:px-2 border-b border-b-slate-300'>{data.nama}</td>
                                <td className='font-bold text-slate-800 text-left md:px-2 border-b border-b-slate-300 py-2'>{data.buku}</td>
                                <td className='font-bold text-slate-800 text-left md:px-2 border-b border-b-slate-300'>{data.no_hp}</td>
                                <td className='font-bold text-slate-800 text-left md:px-2 border-b border-b-slate-300'>{moment(data.createdAt).format("YYYY-MM-DD")}</td>
                                <td className='font-bold text-slate-800 text-left md:px-2 border-b border-b-slate-300'>{data.pinjam_sampai_tgl}</td>
                                <td className='font-bold text-slate-800 md:px-4 border-b border-b-slate-300 cursor-pointer'><HiOutlinePencilSquare className='w-5 h-5' onClick={() => showEdit(data)} /></td>
                            </tr>
                        )) : <h1>a</h1>}
                    </tbody>
                </table>
            </Table>
        </div>
    )
}

export default TablePeminjam