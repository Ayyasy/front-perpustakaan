import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import home from "../assets/home.jpg"
import logo from "../assets/logo.jpg"
import Text from '../component/Text'

function HomeScreen() {
    const [transition, setTransition] = useState(false)
    useEffect(() => {
        setTransition(true)
    }, [])
    return (
        <div className=''>
            <img className='w-screen h-screen absolute blur-[3px] -z-10' src={home} alt="" />
            <div className='flex justify-center h-screen'>
                <div className='md:h-[80vh] md:w-[35vw] w-[80vw] h-[50vh] my-auto flex justify-center md:py-20  rounded-2xl'>
                    <div>
                        <img className='w-32 h-32 rounded-full mx-auto' src={logo} alt="" />
                        <Text className="text-center mt-10 md:text-3xl text-xl text-white animate-pulse" text="Selamat Datang di Perpustakaan Ruang Renung" />
                        <div className='flex justify-center md:mx-20 md:mt-16 mt-20 '>
                            <div className='flex flex-col w-full space-y-9'>
                                <Link to={"/list/buku"} className="text-center w-full  py-4 rounded-lg bg-gradient-to-tr from-slate-800 to-slate-400 hover:from-slate-900 hover:to-slate-500 text-white shadow-xl transition-all ease-in-out duration-500">Pinjam Buku</Link>
                                <Link to={"/register/anggota"} className="text-center w-full  py-4 rounded-lg bg-gradient-to-tr from-slate-800 to-slate-400 hover:from-slate-900 hover:to-slate-500 text-white shadow-xl transition-all ease-in-out duration-500">Daftar Nakama</Link>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default HomeScreen