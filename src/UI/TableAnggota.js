import React, { useEffect, useState } from 'react'
import Admin from '../api/Admin'
import Table from '../component/Table'
import Loading from '../component/Loading'
import Snackbar from '../component/Snackbar'
import Dialog from '../component/Dialog'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from "yup"
import Input from '../component/Input';
import { HiOutlinePencilSquare, HiTrash } from "react-icons/hi2"
import Option from '../component/Option'
import AsyncOption from '../component/AsyncOption'
import { useDispatch, useSelector } from 'react-redux'
import { findAllAnggota, findAnggotaByKey, editAnggotaOne, hapusAnggotaOne } from '../store/action/anggota'
import { getAllBuku } from '../store/action/buku'
import { createPeminjam, findPeminjamByUuid, hapusPeminjamOne } from '../store/action/peminjam'

function TableAnggota() {
    // const [anggota, setAnggota] = useState([])
    const dispatch = useDispatch()
    const { anggota, loading, msgAngt } = useSelector((state) => state.anggota)
    const { allpeminjam, msgPinjam } = useSelector((state) => state.peminjam)
    const { allbuku } = useSelector((state) => state.buku)
    const [msg, setMsg] = useState("")
    const [key, setKey] = useState("")
    const [page, setPage] = useState(1)
    const [textPage, setTextPage] = useState(1)
    const [dialogTambahAnggota, setDialogTambahAnggota] = useState(false)
    const [dialogPinjamBuku, setDialogPinjamBuku] = useState(false)
    const [dialogHapusPinjaman, setDialogHapusPinjaman] = useState(false)
    const [dialogEditAnggota, setDialogEditAnggota] = useState(false)
    const [snackAnggota, setsnackAnggota] = useState(false)
    const [snackPeminjam,setsnackPeminjam] = useState(false)
    const [uuid, setUuid] = useState("")
    const [row, setRow] = useState(5)
    const [totalPage, setTotalPage] = useState([])
    const [buku, setBuku] = useState([])
    const [bukuOption, setBukuOption] = useState("")
    const [bukuLabel, setBukuLabel] = useState("")
    const method = useForm({
        mode: "all",
        resolver: yupResolver(
            yup.object().shape({
                identitas: yup.string().required("Identitas Tidak Boleh Kosong"),
                nama: yup.string().required("Nama Tidak Boleh Kosong"),
                no_hp: yup.string().required("Nomor Hp Tidak Boleh Kosong"),
                alamat: yup.string().required("Alamat Tidak Boleh Kosong")
            })
        )
    })
    const methodPinjam = useForm({
        mode: "all",
        resolver: yupResolver(
            yup.object().shape({
                uuid_anggota: yup.string(),
                uuid_buku: yup.string().required("Tidak Boleh Kosong"),
                tanggalPengembalian: yup.string().required("Tanggal Tidak Boleh Kosong")
            })
        )
    })
    const methodEdit = useForm({
        mode: "all",
        resolver: yupResolver(
            yup.object().shape({
                identitas: yup.string().required("Identitas Tidak Boleh Kosong"),
                nama: yup.string().required("Nama Tidak Boleh Kosong"),
                no_hp: yup.string().required("Nomor Hp Tidak Boleh Kosong"),
                alamat: yup.string().required("Alamat Tidak Boleh Kosong")
            })
        )
    })
    useEffect(() => {
        dispatch(findAllAnggota())
        dispatch(getAllBuku())
        setTotalPage(Math.ceil(anggota.length / row))
    }, [page, row, snackAnggota,snackPeminjam])

    const getAnggotaByKey = () => {
        setPage(1)
        setTextPage(1)
        dispatch(findAnggotaByKey(key.trim()))
    }
    const editAnggota = async (update) => {
        setDialogEditAnggota(false)
        dispatch(editAnggotaOne({ uuid, update }))
        setMsg(msgAngt)
        setsnackAnggota(true)
        setPage(1)
        setTextPage(1)
    }
    const hapusAnggota = async () => {
        setDialogEditAnggota(false)
        dispatch(hapusAnggotaOne(uuid))
        setsnackAnggota(true)
        setPage(1)
        setTextPage(1)
    }
    const addPinjaman = async (data) => {
        setDialogPinjamBuku(false)
        dispatch(createPeminjam(data))
        setsnackPeminjam(true)
    }
    const hapusPeminjam = () => {
        setDialogHapusPinjaman(false)
        dispatch(hapusPeminjamOne(allpeminjam.uuid))
        setsnackPeminjam(true)

    }
    const showTambah = () => {
        setDialogTambahAnggota(!dialogTambahAnggota)
    }
    const nextHandle = () => {
        if (page <= anggota.length - row) {
            setPage((e) => e += row)
            setTextPage((e) => e += 1)
        }

    }
    const prevHandle = () => {
        if (page > 1) {
            setPage((e) => e -= row)
            setTextPage((e) => e -= 1)
        }
    }

    const changeRow = (e) => {
        setPage(1)
        setTextPage(1)
        if (e.target.value === "all") {
            setRow(anggota.length)
        }
        else {
            setRow(parseInt(e.target.value))
        }

    }
    const showPinjamDialog = (e) => {
        setBuku(allbuku.map((e) => {
            return { value: e.uuid, label: e.judul_buku }
        }))
        setDialogPinjamBuku(!dialogPinjamBuku)
        methodPinjam.reset({ uuid_anggota: e.uuid })
    }
    const showDialogDelete = async (e) => {
        dispatch(findPeminjamByUuid(e.uuid))
        setDialogHapusPinjaman(true)
    }
    const showEditDialog = (e) => {
        setDialogEditAnggota(!dialogEditAnggota)
        setUuid(e.uuid)
        methodEdit.reset({ nama: e.nama, identitas: e.identitas, no_hp: e.no_hp, alamat: e.alamat })
    }

    // const bukuByKey = async(e) => {
    //     try{
    //         const res = await Admin.bukuBykey(e)
    //         if(res.status === 200){
    //             return res.data.map((e)=>{
    //                 return {value:e.uuid,label:e.judul_buku}
    //             })
    //         }
    //         return []
    //     }
    //     catch(err){
    //         console.log(err)
    //         return []
    //     }
    // }
    // const loadOptions = async(
    //     inputValue,
    //     callback
    //   ) => {
    //     const buku = await bukuByKey(inputValue)
    //     callback(buku)
    //   };
    const selectChange = (e) => {
        setBukuOption(e.value)
        setBukuLabel(e.label)
    }
    return (
        <div>
            <Loading show={loading} />
            <Snackbar show={snackAnggota} toggle={setsnackAnggota} msg={msgAngt} />
            <Snackbar show={snackPeminjam} toggle={setsnackPeminjam} msg={msgPinjam} />
            {/* <Dialog show={dialogTambahAnggota} toggle={() => {
                method.reset()
                setDialogTambahAnggota(false)
            }}>
                <div className='md:m-10 m-6'>
                    <div>
                        <h1 className='text-center md:text-4xl text-4xl font-light'>Tambah Anggota</h1>
                    </div>
                    <form onSubmit={method.handleSubmit(addAnggota)} className='border border-slate-400 md:px-8 px-4 md:mt-8 mt-9 py-10 flex flex-col justify-evenly'>
                        <div className='w-full space-y-2'>
                            <Input className="py-1 px-2 w-full" name="No Ktp/Sim" method={method} methodName="identitas" />
                            <Input className="py-1 px-2 w-full" name="Nama" method={method} methodName="nama" />
                            <Input className="py-1 px-2 w-full" name="Nomor Hp" method={method} methodName="no_hp" />
                            <Input className="py-1 px-2 w-full" name="Alamat" method={method} methodName="alamat" />
                        </div>
                        <button type='submit' className='w-full mt-10 py-3 rounded-lg text-white bg-gradient-to-tr from-slate-600 to-slate-400 hover:from-slate-700 hover:to-slate-500'>Tambah</button>
                    </form>
                </div>
            </Dialog> */}
            <Dialog show={dialogEditAnggota} toggle={() => {
                methodEdit.reset()
                setDialogEditAnggota(false)
            }}>
                <div className='md:m-10 m-6'>
                    <div>
                        <h1 className='text-center md:text-4xl text-4xl font-light'>Edit Anggota</h1>
                    </div>
                    <div className='border border-slate-400 md:px-8 px-4 md:mt-8 mt-9 py-10'>
                        <form onSubmit={methodEdit.handleSubmit(editAnggota)} className=' flex flex-col'>
                            <div className='w-full space-y-2'>
                                <Input className="py-1 px-2 w-full" name="No Ktp/Sim" method={methodEdit} methodName="identitas" />
                                <Input className="py-1 px-2 w-full" name="Nama" method={methodEdit} methodName="nama" />
                                <Input className="py-1 px-2 w-full" name="Nomor Hp" method={methodEdit} methodName="no_hp" />
                                <Input className="py-1 px-2 w-full" name="Alamat" method={methodEdit} methodName="alamat" />
                            </div>
                            <button type='submit' className='w-full mt-10 py-3 rounded-lg text-white bg-gradient-to-tr from-slate-600 to-slate-400 hover:from-slate-700 hover:to-slate-500'>Edit</button>
                        </form>
                        <button onClick={hapusAnggota} className='w-full mt-3 py-3 rounded-lg text-white bg-gradient-to-tr from-slate-600 to-slate-400 hover:from-slate-700 hover:to-slate-500'>Hapus</button>
                    </div>
                </div>
            </Dialog>
            <Dialog show={dialogPinjamBuku} toggle={() => {
                setBukuOption("")
                setBukuLabel("")
                methodPinjam.reset()
                setDialogPinjamBuku(false)
            }}>
                <div className='md:m-10 m-6'>
                    <div>
                        <h1 className='text-center md:text-4xl text-4xl font-light'>Tambah Pinjaman</h1>
                    </div>
                    <form onSubmit={methodPinjam.handleSubmit(addPinjaman)} className='border border-slate-400 md:px-8 px-4 md:mt-8 mt-9 py-10 flex flex-col justify-evenly'>
                        <div className='w-full space-y-2'>
                            {/* <Input className="py-1 px-2 w-full" name="Nama" method={methodPinjam} methodName="nama" readOnly={true} /> */}
                            {/* <AsyncSelect cacheOptions loadOptions={loadOptions} defaultOptions /> */}
                            <Option method={methodPinjam} methodName="uuid_buku" options={buku} label="buku" onChange={(e) => selectChange(e)} value={{ value: bukuOption, label: bukuLabel }} />
                            {/* <AsyncOption loadOptions={loadOptions}  method={methodPinjam} methodName="buku" label="Buku" placeholder="Pilih Buku"/> */}
                            {/* <Input className="py-1 px-2 w-full" name="Nomor Hp" method={methodPinjam} methodName="nomorHp" readOnly={true} /> */}
                            <Input className="py-1 px-2 w-full" name="Tanggal Pengembalian" method={methodPinjam} type="date" methodName="tanggalPengembalian" />
                        </div>
                        <button type='submit' className='w-full mt-10 py-3 rounded-lg text-white bg-gradient-to-tr from-slate-600 to-slate-400 hover:from-slate-700 hover:to-slate-500'>Tambah</button>
                    </form>
                </div>
            </Dialog>
            <Dialog show={dialogHapusPinjaman} toggle={setDialogHapusPinjaman} classExit="hidden">
                <div className='md:m-10 m-6'>
                    <div className='mb-5'>
                        <h1 className='text-center md:text-4xl text-4xl font-light'>Pengembalian Buku</h1>
                    </div>
                    <div className='border border-slate-400 px-5 py-5 space-y-3 text-xl w-full'>
                        <h1>Buku yang dipinjam : {allpeminjam.buku}</h1>
                        <h1>Batas Peminjaman : {allpeminjam.pinjam_sampai_tgl}</h1>
                    </div>
                    <div className='flex justify-end mt-7 space-x-3'>
                        <button className='bg-red-700 px-7 py-1 text-white rounded-md' onClick={hapusPeminjam}>Ya</button>
                        <button className='bg-blue-700 px-7 py-1 text-white rounded-md' onClick={() => setDialogHapusPinjaman(false)}>Batal</button>
                    </div>
                </div>
            </Dialog>
            <Table
                title={"Daftar Anggota"}
                value={key}
                onChange={(e) => setKey(e.target.value)}
                getByKey={getAnggotaByKey}
                titleTable={"List Anggota"}
                titleAdd={"Tambah Anggota"}
                searchTitle={"Nama/identitas"}
                textPage={textPage}
                prevHandle={prevHandle}
                nextHandle={nextHandle}
                dialogTambah={showTambah}
                row={row}
                changeRow={changeRow}
                totalPage={totalPage}
                classTambah="hidden"

            >
                <table>
                    <thead className='bg-[#fedcba]'>
                        <tr className='text-lg'>
                            <th className='md:px-5 md:py-3 font-semibold px-5'>No</th>
                            <th className='font-semibold text-left md:px-2 md:w-[20rem] px-10'>Identitas</th>
                            <th className='font-semibold text-left md:px-2 md:w-[25rem] pr-56'>Nama</th>
                            {/* <th className='font-semibold text-left md:px-2 md:w-[15rem] w-[15rem] px-10'>Nomor Hp</th> */}
                            <th className='font-semibold text-left md:px-2 md:w-[18rem] pr-56'>Alamat</th>
                            <th className='font-semibold text-left md:px-2 md:w-[9rem] px-5'>Edit</th>
                            <th className='font-semibold text-left md:px-2 md:w-[9rem] px-5'>Pinjam</th>
                            <th className='font-semibold text-left md:px-2 md:w-[9rem] px-5'>Kembalikan</th>
                        </tr>
                    </thead>
                    <tbody>
                        {anggota.slice(page - 1, page + (row - 1)).map((data, id) => (
                            <tr key={id} className={`uppercase bg-white`}>
                                <td className='font-bold text-slate-800 md:px-5 md:py-3 border-b border-b-slate-300 border-l px-10'>{id + page}</td>
                                <td className='font-bold text-slate-800 text-left md:px-2 border-b border-b-slate-300 px-10'>{data.identitas}</td>
                                <td className='font-bold text-slate-800 text-left md:px-2 border-b border-b-slate-300 py-1 '>{data.nama}</td>
                                <td className='font-bold text-slate-800 text-left md:px-2 border-b border-b-slate-300 px-10'>{data.alamat}</td>
                                <td className='font-bold text-slate-800 md:px-4 border-b border-b-slate-300 cursor-pointer'><HiOutlinePencilSquare className='w-7 h-7 md:mx-0 mx-auto' onClick={() => showEditDialog(data)} /></td>
                                {/* <td className='font-bold text-slate-800 text-left md:px-2 border-b border-b-slate-300 px-10'>{data.no_hp}</td> */}
                                <td className='font-bold text-slate-800 md:px-4 border-b border-b-slate-300 cursor-pointer'><HiOutlinePencilSquare className='w-7 h-7 md:mx-2 mx-auto' onClick={() => showPinjamDialog(data)} /></td>
                                <td className='font-bold text-slate-800 md:px-4 border-b border-b-slate-300 cursor-pointer'><HiOutlinePencilSquare onClick={() => showDialogDelete(data)} className='w-7 h-7 md:mx-7 mx-auto' /></td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </Table>
        </div>
    )
}

export default TableAnggota