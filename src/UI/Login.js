import React, { useState } from 'react'
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup"
import * as yup from "yup"
import Input from '../component/Input';
import Button from '../component/Button';
import Admin from '../api/Admin';
import { useDispatch, useSelector } from 'react-redux';
import background from "../assets/bg.jpg"
import logo from "../assets/logo.jpg"
import Text from '../component/Text';
import { VscEye, VscEyeClosed } from "react-icons/vsc";
import { ImUser, ImLock } from "react-icons/im"
import Loading from '../component/Loading';
import Snackbar from '../component/Snackbar';
import { userActions } from '../store/slice/user';
import { login } from "../store/action/user"

function Login() {
    const [eye, setEye] = useState(false)
    const [snackBar, setsnackBar] = useState(false)
    const dispatch = useDispatch()
    const { loading,msg } = useSelector((state) => state.user)
    const method = useForm({
        mode: "all",
        resolver: yupResolver(
            yup.object().shape({
                username: yup.string().required("Username Tidak Boleh Kosong"),
                password: yup.string().required("Password Tidak Boleh Kosong").min(8, "Password Minimal 8 Karakater")
            })
        )
    })
    const isLogin =  (data) => {
        dispatch(login(data))
        setsnackBar(true)
    }
    return (
        <div className='overflow-x-hidden'>
            <Loading show={loading} />
            <Snackbar show={snackBar} toggle={setsnackBar} msg={msg} />
            <img className='absolute w-full h-screen md:blur-0 blur-sm' src={background} alt="" />
            <form className='relative md:w-[30vw] md:h-[100vh] flex flex-col md:justify-center md:ml-10 md:mx-0 mx-3' onSubmit={method.handleSubmit(isLogin)}>
                <div className='md:flex md:space-x-3'>
                    <img className='md:w-12 md:h-12 w-40 h-40 mr-3 rounded-full md:mx-0 mx-[30%] md:mt-0 mt-14' src={logo} alt="" />
                    <Text className="md:text-[3vmin] text-xl mb-5 md:text-left text-center md:mt-0 mt-12" text="Selamat Datang di Perpustakaan Ruang Renung" />
                </div>
                <div className='space-y-8 mb-8'>
                    <Input
                        className="w-full py-2 px-3 "
                        tag={<ImUser className='w-6 h-6' />}
                        name="username"
                        method={method}
                        methodName="username"
                    />
                    <Input
                        className="w-full py-2 px-3 "
                        tag={<ImLock className='w-6 h-6' />}
                        type={eye ? "text" : "password"}
                        name="Password"
                        method={method}
                        methodName="password"
                        icon={eye ? <VscEye className='w-6 h-6 -ml-8 cursor-pointer absolute right-2 mt-2' onClick={() => setEye(!eye)} /> : <VscEyeClosed className='w-6 h-6 my-auto -ml-8 cursor-pointer absolute right-2 mt-2' onClick={() => setEye(!eye)} />}
                    />

                </div>
                <Button
                    className="from-sky-600 to-sky-300 py-3 shadow-xl hover:from-sky-700 hover:to-sky-400 bg-gradient-to-bl"
                    name="Sign in"
                    type="submit"
                />
            </form>

        </div>
    )
}

export default Login