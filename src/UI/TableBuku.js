import React, { useEffect, useState } from 'react'
import Admin from '../api/Admin'
import { HiOutlinePencilSquare, HiOutlineTrash } from "react-icons/hi2"
import Dialog from '../component/Dialog'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from "yup"
import Input from '../component/Input'
import Snackbar from '../component/Snackbar'
import Loading from '../component/Loading'
import moment from "moment"
import Option from '../component/Option'
import Table from '../component/Table'
import { useDispatch, useSelector } from 'react-redux'
import { deleteOneBuku, editOneBuku, getAllBuku, getByKey, postNewBuku } from "../store/action/buku"

function TableBuku() {
    const [key, setKey] = useState("")
    const [page, setPage] = useState(1)
    const [textPage, setTextPage] = useState(1)
    const [dialog, setDialog] = useState(false)
    const [dialogTambah, setDialogTambah] = useState(false)
    const [dialogHapus, setDialogHapus] = useState(false)
    const [snack, setsnack] = useState(false)
    const [uuid, setUuid] = useState("")
    const [row, setRow] = useState(5)
    const [totalPage, setTotalPage] = useState([])
    const [valuePemilik, setValuePemilik] = useState("")
    const { allbuku, loading, msg } = useSelector((state) => state.buku)
    const dispatch = useDispatch()
    const method = useForm({
        mode: "all",
        resolver: yupResolver(
            yup.object().shape({
                judul: yup.string().required("Judul Buku Harus di Isi"),
                tipe: yup.string().required("Kategori Harus di Isi"),
                pengarang: yup.string().required("Pengarang Harus di Isi"),
                iso: yup.string(),
                pemilik: yup.string().required("Pemilik Tidak Boleh Kosong"),
                jumlah: yup.string().required("Jumlah Harus di isi").matches(/^\d+$/, "Wajib di isi").min(1, "Jumlah buku Harus di isi")
            })
        )
    })
    const methodEdit = useForm({
        mode: "all",
        resolver: yupResolver(
            yup.object().shape({
                judul: yup.string().required("Judul Buku Harus di Isi"),
                tipe: yup.string().required("Kategori Harus di Isi"),
                pengarang: yup.string().required("Pengarang Harus di Isi"),
                iso: yup.string(),
                pemilik: yup.string().required("Pemilik Tidak Boleh Kosong"),
                jumlah: yup.string().required("Jumlah Harus di isi").matches(/^\d+$/, "Wajib di isi").min(1, "Jumlah buku Harus di isi")

            })
        )
    })
    useEffect(() => {
        dispatch(getAllBuku())
        setTotalPage(Math.ceil(allbuku.length / row))
    }, [row, page,snack])
    const cariByKey = () => {
        if (key.trim !== "") {
            dispatch(getByKey(key))
        }
        else {
            dispatch(getAllBuku())
        }
        setPage(1)
        setTextPage(1)
    }
    const tambahBuku = async (add) => {
        setDialogTambah(false)
        dispatch(postNewBuku(add))
        setsnack(true)
        setValuePemilik("")
        method.reset()

    }
    const editBuku = async (update) => {
        setDialog(false)
        dispatch(editOneBuku({ uuid, update }))
        setsnack(true)

    }
    const hapusBuku = async () => {
        setDialogHapus(false)
        dispatch(deleteOneBuku(uuid))
        setsnack(true)
    }
    const showHapus = (e) => {
        setUuid(e)
        setDialogHapus(!dialogHapus)
    }
    const showEdit = (e) => {
        setDialog(!dialog)
        setUuid(e.uuid)
        methodEdit.reset({ judul: e.judul_buku, tipe: e.tipe_buku, pengarang: e.pengarang, iso: e.iso, pemilik: e.pemilik, jumlah: e.jumlah_buku })

    }
    const showTambah = () => {
        setDialogTambah(!dialogTambah)
    }
    const nextHandle = () => {
        if (page <= allbuku.length - row) {
            setPage((e) => e += row)
            setTextPage((e) => e += 1)
        }

    }
    const prevHandle = () => {
        if (page > 1) {
            setPage((e) => e -= row)
            setTextPage((e) => e -= 1)
        }
    }
    const optionPemilik = [
        { value: "Ruang Renung", label: "Ruang Renung" },
        { value: "Perjal Minor", label: "Perjal Minor" }
    ]
    const changeRow = (e) => {
        setPage(1)
        setTextPage(1)
        if (e.target.value === "all") {
            setRow(allbuku.length)
        }
        else {
            setRow(parseInt(e.target.value))
        }
    }
    const pemilikSelect = (e) => {
        setValuePemilik(e.value)
    }
    return (
        <div className='overflow-x-hidden'>
            <Loading show={loading} />
            <Snackbar show={snack} toggle={setsnack} msg={msg} />
            <Dialog show={dialog} toggle={() => {
                methodEdit.reset({ judul: "", tipe: "", pengarang: "", iso: "", pemilik: { value: "", label: "" }, jumlah_buku: 0 })
                setDialog(false)
            }}>
                <div className='md:m-7 m-6'>
                    <div>
                        <h1 className='text-center md:text-4xl text-4xl font-light'>Edit Buku</h1>
                    </div>
                    <form onSubmit={methodEdit.handleSubmit(editBuku)} className='border border-slate-400 md:px-8 px-4 md:mt-6 mt-9 py-10 flex flex-col justify-evenly'>
                        <div className='w-full space-y-2'>
                            <Input className="py-1 px-2 w-full" name="Judul" method={methodEdit} methodName="judul" />
                            <Input className="py-1 px-2 w-full" name="Kategori" method={methodEdit} methodName="tipe" />
                            <Input className="py-1 px-2 w-full" name="Pengarang" method={methodEdit} methodName="pengarang" />
                            <Input className="py-1 px-2 w-full" name="iso" method={methodEdit} methodName="iso" />
                            <Input className="py-1 px-2 w-full" name="jumlah buku" type="number" method={methodEdit} methodName="jumlah" />
                            <Option label="Pemilik" options={optionPemilik} method={methodEdit} methodName="pemilik" />
                        </div>
                        <button type='submit' className='w-full mt-10 py-3 rounded-lg text-white bg-gradient-to-tr from-slate-600 to-slate-400 hover:from-slate-700 hover:to-slate-500'>Edit</button>
                    </form>
                </div>
            </Dialog>
            <Dialog show={dialogTambah} toggle={() => {
                setValuePemilik("")
                method.reset()
                setDialogTambah(false)
            }
            }>
                <div className='md:m-7 m-6'>
                    <div>
                        <h1 className='text-center md:text-4xl text-4xl font-light'>Tambah Buku</h1>
                    </div>
                    <form onSubmit={method.handleSubmit(tambahBuku)} className='border border-slate-400 md:px-8 px-4 md:mt-6 mt-9 py-10 flex flex-col justify-evenly'>
                        <div className='w-full space-y-2'>
                            <Input className="py-1 px-2 w-full" name="Judul" method={method} methodName="judul" />
                            <Input className="py-1 px-2 w-full" name="Kategori" method={method} methodName="tipe" />
                            <Input className="py-1 px-2 w-full" name="Pengarang" method={method} methodName="pengarang" />
                            <Input className="py-1 px-2 w-full" name="iso" method={method} methodName="iso" />
                            <Input className="py-1 px-2 w-full" name="jumlah buku" type="number" method={method} methodName="jumlah" />
                            <Option label="Pemilik" options={optionPemilik} method={method} methodName="pemilik" onChange={(e) => pemilikSelect(e)} value={{ value: valuePemilik, label: valuePemilik }} placeholder="Pemilik" />
                        </div>
                        <button type='submit' className='w-full mt-10 py-3 rounded-lg text-white bg-gradient-to-tr from-slate-600 to-slate-400 hover:from-slate-700 hover:to-slate-500'>Tambah</button>
                    </form>
                </div>
            </Dialog>
            <Dialog show={dialogHapus} toggle={setDialogHapus} classExit="hidden">
                <div className='m-5'>
                    <div className='mx-2 my-5 text-2xl'>
                        <h1>Apakah Anda Yakin Untuk Menghapus Buku ini?</h1>
                    </div>
                    <div className='flex justify-end space-x-3'>
                        <button className='bg-red-700 px-7 py-1 text-white rounded-md' onClick={hapusBuku}>Ya</button>
                        <button className='bg-blue-700 px-7 py-1 text-white rounded-md' onClick={() => setDialogHapus(false)}>Tidak</button>
                    </div>
                </div>
            </Dialog>
            <Table
                title={"Daftar Buku"}
                value={key}
                onChange={(e) => setKey(e.target.value)}
                getByKey={cariByKey}
                titleTable={"List Buku"}
                titleAdd={"Tambah Buku"}
                searchTitle={"Judul/Pengarang"}
                textPage={textPage}
                page={page}
                prevHandle={prevHandle}
                nextHandle={nextHandle}
                dialogTambah={showTambah}
                row={row}
                changeRow={changeRow}
                totalPage={totalPage}
            >
                <table>
                    <thead className='bg-[#fedcba]'>
                        <tr className='text-lg'>
                            <th className='md:px-5 md:py-3 font-semibold px-5'>No</th>
                            <th className='md:px-5 md:py-3 font-semibold pr-5'>Jumlah</th>
                            <th className='font-semibold text-left md:px-2 md:w-[30rem] pr-72'>Judul</th>
                            <th className='font-semibold text-left md:px-2 md:w-[16rem] pl-4 pr-32'>Kategori</th>
                            <th className='font-semibold text-left md:px-2 md:w-[20rem] pl-4 pr-32'>Pengarang</th>
                            <th className='font-semibold text-left md:px-2 md:w-[18rem] pl-4 pr-32'>Iso</th>
                            <th className='font-semibold text-left  md:w-[13rem] pl-4 pr-32'>Pemilik</th>
                            <th className='font-semibold text-left md:px-10 md:w-[6rem] px-10'>Edit</th>
                            <th className='font-semibold text-left md:px-10 md:w-[6rem] px-10'>Hapus</th>
                        </tr>
                    </thead>
                    <tbody>
                        {allbuku.slice(page - 1, page + (row - 1)).map((data, id) => (
                            <tr key={id} className={`uppercase bg-white`}>
                                <td className='font-bold text-slate-800 md:px-5 md:py-3 border-b border-b-slate-300 py-3 border-l px-5'>{id + page}</td>
                                <td className='font-bold text-slate-800 text-left md:px-10 px-4 border-b border-b-slate-300 py-3'>{data.jumlah_buku}</td>
                                <td className='font-bold text-slate-800 text-left md:px-2 border-b border-b-slate-300 py-3'>{data.judul_buku}</td>
                                <td className='font-bold text-slate-800 text-left md:px-2 border-b border-b-slate-300 py-3 pl-4'>{data.tipe_buku}</td>
                                <td className='font-bold text-slate-800 text-left md:px-2 border-b border-b-slate-300 py-3 pl-4'>{data.pengarang}</td>
                                <td className='font-bold text-slate-800 text-left md:px-2 border-b border-b-slate-300 py-3 pl-4'>{data.iso === null || data.iso === "" ? "-" : data.iso}</td>
                                <td className='font-bold text-slate-800 text-left md:px-2 border-b border-b-slate-300 py-3 pl-4'>{data.pemilik}</td>
                                <td className='font-bold text-slate-800 md:px-4 border-b border-b-slate-300 cursor-pointer py-3 '><HiOutlinePencilSquare className='w-6 h-6 mx-auto' onClick={() => showEdit(data)} /></td>
                                <td className='font-bold text-slate-800 md:px-6 border-b border-b-slate-300 cursor-pointer py-3'><HiOutlineTrash onClick={() => showHapus(data.uuid)} className='w-6 h-6 mx-auto' /></td>
                            </tr>
                        ))}
                    </tbody>
                </table>

            </Table>
        </div>
    )
}

export default TableBuku