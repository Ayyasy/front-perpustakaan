import { createAsyncThunk } from "@reduxjs/toolkit";
import Admin from "../../api/Admin";

export const findAllAnggota = createAsyncThunk(
    'anggota/all',
    async(_,{rejectWithValue})=>{
        try{
            const res  = await Admin.allAnggota()
            if(res.status === 200){
                return res.data
            }
        }
        catch(_){
            return rejectWithValue("not found")
        }
    }
)
export const findAnggotaByKey =  createAsyncThunk(
    'anggota/key',
    async(key,{rejectWithValue})=>{
        try{
            const res = await Admin.anggotaByKey(key)
            if(res.status === 200){
                return res.data
            }
        }
        catch(_){
          return rejectWithValue("not found")  
        }
    }
)
export const createAnggota = createAsyncThunk(
    'anggota/add',
    async(data,{rejectWithValue})=>{
        try{
            const res = await Admin.tambahAnggota(data)
            if(res.status === 200){
                return res.data
            }
        }
        catch(_){
            return rejectWithValue("Tidak Bisa Mendaftar")
        }
    }
)
export const editAnggotaOne = createAsyncThunk(
    'anggota/edit',
    async(data,{rejectWithValue}) => {
        try{
            const res = await Admin.updateAnggota(data.uuid,data.update)
            if(res.status === 200){
                return res.data
            }
        }
        catch(_){
            return rejectWithValue("Gagal")
        }
    }
)
export const hapusAnggotaOne =  createAsyncThunk(
    'anggota/delete',
    async(uuid,{rejectWithValue})=>{
        try{
            const res = await Admin.deleteAnggota(uuid)
            if(res.status === 200){
                return res.data
            }
        }
        catch(_){
            return rejectWithValue("Gagal")
        }
    }
)