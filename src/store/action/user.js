import { createAsyncThunk } from "@reduxjs/toolkit";
import Admin from "../../api/Admin"

export const login = createAsyncThunk(
    'user/login',
    async(data,{rejectWithValue}) => {
        try{
            const res = await Admin.login(data)
            if(res.status === 200){
                return res.data
            }
        }
        catch(_){
            return rejectWithValue("username atau password salah")
        }
    }
)
export const logout = createAsyncThunk(
    'user/logout',
    async(_,{rejectWithValue})=>{
        try{
            await Admin.logout()
        }
        catch(_){
            return rejectWithValue("error")
        }
    }
)