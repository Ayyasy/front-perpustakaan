import { createAsyncThunk } from "@reduxjs/toolkit";
import Admin from "../../api/Admin";

export const getAllBuku = createAsyncThunk(
    'buku/all',
    async(_,{rejectWithValue})=>{
        try{
            const res = await Admin.allBuku()
            if(res.status === 200){
                return res.data
            }
        }
        catch(_){
            return rejectWithValue("error")
        }
    }
)
export const postNewBuku = createAsyncThunk(
    'buku/post',
    async(data,{rejectWithValue})=>{
        try{
            const res = await Admin.postBuku(data)
            if(res.status === 200){
                return res.data
            }
        }
        catch(_){
            return rejectWithValue("error")
        }
    }
)
export const getByKey = createAsyncThunk(
    'buku/key',
    async(key,{rejectWithValue})=>{
        try{
            const res = await Admin.bukuBykey(key)
            if(res.status === 200){
                return res.data
            }
        }
        catch(_){
            return rejectWithValue("Not Found")
        }
    }
)
export const deleteOneBuku = createAsyncThunk(
    'buku/delete',
    async(uuid,{rejectWithValue})=>{
        try{
            const res = await Admin.deleteBuku(uuid)
            if(res.status === 200){
                return res.data
            }
        }
        catch(_){
            return rejectWithValue("error")
        }
    }
)
export const editOneBuku = createAsyncThunk(
    'buku/update',
    async(data,{rejectWithValue})=>{
        try{
            const res = await Admin.updateBuku(data.uuid,data.update)
            if(res.status === 200){
                return res.data
            }
        }
        catch(_){
            return rejectWithValue("error")
        }
    }
)