import { createAsyncThunk } from "@reduxjs/toolkit";
import Admin from "../../api/Admin";

export const findAllPeminjam = createAsyncThunk(
    'peminjam/all',
    async (_, { rejectWithValue }) => {
        try {
            const res = await Admin.allPeminjam()
            if (res.status === 200) {
                return res.data
            }
        }
        catch (_) {
            return rejectWithValue("not found")
        }
    }
)
export const findPeminjamByKey = createAsyncThunk(
    'peminjam/key',
    async (key, { rejectWithValue }) => {
        try {
            const res = await Admin.peminjmanByNama(key)
            if (res.status === 200) {
                return res.data
            }
        }
        catch (_) {
            return rejectWithValue("not found")
        }
    }
)
export const findPeminjamByUuid = createAsyncThunk(
    'peminjam/uuid',
    async (uuid, { rejectWithValue }) => {
        try {
            const res = await Admin.peminjamByUuid(uuid)
            if (res.status === 200) {
                if (res.data === null || res.data === undefined) {
                    return []
                }
                return res.data
            }
        }
        catch (_) {
            return rejectWithValue("not found")
        }
    }
)
export const createPeminjam = createAsyncThunk(
    'peminjam/add',
    async (data, { rejectWithValue }) => {
        try {
            const res = await Admin.tambahPeminjam(data)
            if (res.status === 200) {
                return res.data
            }
        }
        catch (_) {
            return rejectWithValue("Anggota Sudah Pernah Meminjam Sebelumnya")
        }
    }
)
export const editPeminjamOne = createAsyncThunk(
    'peminjam/update',
    async (data, { rejectWithValue }) => {
        try {
            const res = await Admin.updatePeminjam(data.uuid, data.update)
            if (res.status === 200) {
                return res.data
            }
        }
        catch (_) {
            return rejectWithValue("Gagal")
        }
    }
)
export const hapusPeminjamOne = createAsyncThunk(
    'peminjam/delete',
    async (uuid, { rejectWithValue }) => {
        try {
            const res = await Admin.deletePeminjam(uuid)
            if (res.status === 200) {
                return res.data
            }
        }
        catch (_) {
            return rejectWithValue("Anggota Tidak Pernah Meminjam Sebelumnya")
        }
    }
)