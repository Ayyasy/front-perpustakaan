import { createSlice } from "@reduxjs/toolkit";
import { login, logout } from "../action/user"

export const userStore = createSlice({
    name: "user",
    initialState: {
        aksesToken: "",
        super_admin: false,
        username: "",
        loading: false,
        msg : ""
    },
    reducers: {
        setAksesToken: (user, action) => {
            user.aksesToken = action.payload
        },
        setSuper_admin: (user, action) => {
            user.super_admin = action.payload
        },
        setUsername: (user, action) => {
            user.username = action.payload
        },
        logout: (user) => {
            user.aksesToken = ""
            user.super_admin = false
            user.username = ""
        }
    },
    extraReducers: builder => {
        builder.addCase(login.pending, (state) => {
            state.loading = true
        })
            .addCase(login.fulfilled, (state, action) => {
                state.loading = false
                state.aksesToken = action.payload.token
                state.super_admin = action.payload.super
                state.username = action.payload.username
            })
            .addCase(login.rejected, (state,action) => {
                state.loading = false
                state.msg = action.payload

            })
            .addCase(logout.pending, (state) => {
                state.loading = true
            })
            .addCase(logout.rejected, (state) => {
                state.loading = false
            })
            .addCase(logout.fulfilled, (state) => {
                state.loading = false
                state.aksesToken = ""
                state.super_admin = ""
                state.username = ""
            })
    }
})
export const userActions = userStore.actions
export default userStore.reducer