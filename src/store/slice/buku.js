import { createSlice } from "@reduxjs/toolkit";
import { deleteOneBuku, editOneBuku, getAllBuku, getByKey, postNewBuku } from "../action/buku";


export const bukuStore = createSlice({
    name: "buku",
    initialState: {
        allbuku: [],
        loading: false,
        msg : ""
    },
    extraReducers: builder => {
        builder.addCase(getAllBuku.pending, (state) => {
            state.loading = true
        })
            .addCase(getAllBuku.fulfilled, (state, action) => {
                state.loading = false
                state.allbuku = action.payload
            })
            .addCase(getAllBuku.rejected, (state) => {
                state.loading = false
            })
            .addCase(postNewBuku.pending,(state)=>{
                state.loading = true
            })
            .addCase(postNewBuku.fulfilled,(state,action)=>{
                state.loading = false
                state.msg = action.payload.msg
            })
            .addCase(postNewBuku.rejected,(state)=>{
                state.loading = false
            })
            .addCase(getByKey.pending,(state)=>{
                state.loading = true
            })
            .addCase(getByKey.fulfilled,(state,action)=>{
                state.loading = false
                state.allbuku = action.payload
            })
            .addCase(getByKey.rejected,(state)=>{
                state.loading =false
            })
            .addCase(deleteOneBuku.pending,(state)=>{
                state.loading = true
            })
            .addCase(deleteOneBuku.fulfilled,(state,action)=>{
                state.loading = false
                state.msg = action.payload.msg
            })
            .addCase(deleteOneBuku.rejected,(state)=>{
                state.loading = false
            })
            .addCase(editOneBuku.pending,(state)=>{
                state.loading = true
            })
            .addCase(editOneBuku.fulfilled,(state,action)=>{
                state.loading = false
                state.msg = action.payload.msg
            })
            .addCase(editOneBuku.rejected,(state)=>{
                state.loading = false
            })
    }
})
export const bukuActions = bukuStore.actions
export default bukuStore.reducer
