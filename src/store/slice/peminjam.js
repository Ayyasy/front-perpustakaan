import { createSlice } from "@reduxjs/toolkit";
import { findAllPeminjam, findPeminjamByKey, findPeminjamByUuid, createPeminjam, editPeminjamOne, hapusPeminjamOne } from "../action/peminjam";

export const peminjamStore = createSlice({
    name: "peminjam",
    initialState: {
        allpeminjam: [],
        loading: false,
        msgPinjam: ""
    },
    extraReducers: builder => {
        builder.addCase(findAllPeminjam.pending, (state) => { state.loading = true })
            .addCase(findAllPeminjam.fulfilled, (state, action) => {
                state.loading = false
                state.allpeminjam = action.payload
            })
            .addCase(findAllPeminjam.rejected, (state) => { state.loading = false })
            .addCase(findPeminjamByKey.pending, (state) => { state.loading = true })
            .addCase(findPeminjamByKey.fulfilled, (state, action) => {
                state.loading = false
                state.allpeminjam = action.payload
            })
            .addCase(findPeminjamByKey.rejected, (state) => { state.loading = false })
            .addCase(findPeminjamByUuid.pending, (state) => { state.loading = true })
            .addCase(findPeminjamByUuid.fulfilled, (state, action) => {
                state.loading = false
                state.allpeminjam = action.payload
            })
            .addCase(findPeminjamByUuid.rejected, (state) => { state.loading = false })
            .addCase(createPeminjam.pending, (state) => { state.loading = true })
            .addCase(createPeminjam.fulfilled, (state, action) => {
                state.loading = false
                state.msgPinjam = action.payload.msg
            })
            .addCase(createPeminjam.rejected, (state,action) => { 
                state.loading = false
                state.msgPinjam = action.payload
            })
            .addCase(editPeminjamOne.pending, (state) => { state.loading = true })
            .addCase(editPeminjamOne.fulfilled, (state, action) => {
                state.loading = false
                state.msgPinjam = action.payload.msg
            })
            .addCase(editPeminjamOne.rejected, (state,action) => {
                state.loading = false
                state.msgPinjam = action.payload
            })
            .addCase(hapusPeminjamOne.pending, (state) => { state.loading = true })
            .addCase(hapusPeminjamOne.fulfilled, (state, action) => {
                state.loading = false
                state.msgPinjam = action.payload.msg
            })
            .addCase(hapusPeminjamOne.rejected, (state, action) => {
                state.loading = false
                state.msgPinjam = action.payload
            })
    }
})
export default peminjamStore.reducer