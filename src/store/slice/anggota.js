import { createSlice } from "@reduxjs/toolkit";
import { createAnggota, editAnggotaOne, findAllAnggota,findAnggotaByKey, hapusAnggotaOne } from "../action/anggota";

export const anggotaStore = createSlice({
    name : "anggota",
    initialState:{
        anggota:[],
        loading : false,
        msgAngt:""
    },
    extraReducers: builder => {
        builder.addCase(findAllAnggota.pending,(state)=>{
            state.loading = true
        })
        .addCase(findAllAnggota.fulfilled,(state,action)=>{
            state.loading = false
            state.anggota = action.payload
        })
        .addCase(findAllAnggota.rejected,(state)=>{
            state.loading = false
        })
        .addCase(findAnggotaByKey.pending,(state)=>{
            state.loading = true
        })
        .addCase(findAnggotaByKey.fulfilled,(state,action)=>{
            state.loading = false
            state.anggota = action.payload
        })
        .addCase(findAnggotaByKey.rejected,(state)=>{
            state.loading = false
        })
        .addCase(createAnggota.pending,(state)=>{
            state.loading = true
        })
        .addCase(createAnggota.fulfilled,(state,action)=>{
            state.loading = false
            state.msgAngt = action.payload.msg
        })
        .addCase(createAnggota.rejected,(state,action) =>{
            state.loading = false
            state.msgAngt = action.payload
        })
        .addCase(editAnggotaOne.pending,(state)=>{
            state.loading = true
        })
        .addCase(editAnggotaOne.fulfilled,(state,action)=>{
            state.loading = false
            state.msgAngt = action.payload.msg
        })
        .addCase(editAnggotaOne.rejected,(state,action)=>{
            state.loading = false
            state.msgAngt = action.payload
        })
        .addCase(hapusAnggotaOne.pending,(state)=>{
            state.loading = true
        })
        .addCase(hapusAnggotaOne.fulfilled,(state,action)=>{
            state.loading = false
            state.msgAngt = action.payload.msg
        })
        .addCase(hapusAnggotaOne.rejected,(state,action)=>{
            state.loading = false
            state.msgAngt = action.payload
        })
    }
})
export const anggotaActions = anggotaStore.actions
export default anggotaStore.reducer