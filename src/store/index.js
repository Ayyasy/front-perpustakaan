import { configureStore } from "@reduxjs/toolkit";
import { persistReducer,persistStore } from "redux-persist";
import storage from "redux-persist/lib/storage";
import thunk from "redux-thunk";
import anggotaStore from "./slice/anggota";
import bukuStore from "./slice/buku";
import peminjamStore from "./slice/peminjam";
import userStore from "./slice/user";

export const store = configureStore({
    reducer: {
        user : persistReducer({
            key : "Perspustakaan",
            storage
        },userStore),
        buku:bukuStore,
        anggota:anggotaStore,
        peminjam:peminjamStore
    },
    middleware : [thunk]
})
export const persistor = persistStore(store)