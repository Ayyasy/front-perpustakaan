import './App.css';
import Login from './UI/Login';
import { Routes, Route, Navigate } from "react-router-dom"
import HomeScreen from './UI/HomeScreen';
import HomeAdmin from './UI/HomeAdmin';
import TableBuku from './UI/TableBuku';
import { useSelector } from 'react-redux';
import TablePeminjam from './UI/TablePeminjam';
import TableAnggota from './UI/TableAnggota';
import ListBuku from './UI/ListBuku';
import DaftarAnggota from './UI/DaftarAnggota';

function App() {
  const token = useSelector((state) => state.user.aksesToken)
  return (
    <div>
      <Routes>
        <Route path='/' element={<HomeScreen />} />
        <Route path='/list/buku' element={<ListBuku/>}/>
        <Route path='/register/anggota' element={<DaftarAnggota/>}/>
        <Route path='/admin' element={token ? <Navigate to={"/home/daftar-buku"} replace /> : <Login />} />
        <Route path='/home' element={token ? <HomeAdmin /> : <Navigate to={"/admin"} replace />}>
          <Route path='/home/daftar-buku' element={token ? <TableBuku /> : <Navigate to={"/admin"} replace />} />
          <Route path='/home/daftar-peminjam' element={token ? <TablePeminjam /> : <Navigate to={"/admin"} replace />} />
          <Route path='/home/daftar-anggota' element={token? <TableAnggota/> : <Navigate to={"/admin"} replace />} />
        </Route>
        {/* <Route path='/dialog' element={<Dialog />} /> */}
      </Routes>
    </div>
  );
}

export default App;
