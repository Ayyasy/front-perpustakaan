import api from "./Http";

const Admin = {
    login : (data) => {
        return api.post(`/admin/login`,data)
    },
    logout : () => {
        return api.get(`/admin/logout`)
    },
    allBuku : () => {
        return api.get(`/buku/all`)
    },
    bukuBykey : (key) => {
        return api.get(`/buku/all/${key}`)
    },
    postBuku : (data) => {
        return api.post(`/buku/add`,data)
    },
    deleteBuku : (uuid) => {
        return api.delete(`/buku/delete/${uuid}`)
    },
    updateBuku : (uuid,data) => {
        return api.put(`/buku/update/${uuid}`,data)
    },
    allPeminjam : () => {
        return api.get(`/peminjam/all`)
    },
    peminjmanByNama : (nama) => {
        return api.get(`/peminjam/all/${nama}`)
    },
    peminjamByUuid : (uuid) => {
        return api.get(`/peminjam/one/${uuid}`)
    },
    tambahPeminjam : (data) => {
        return api.post(`/peminjam/add`,data)
    },
    deletePeminjam : (uuid) => {
        return api.delete(`/peminjam/delete/${uuid}`)
    },
    updatePeminjam : (uuid,data) => {
        return api.put(`/peminjam/update/${uuid}`,data)
    },
    allAnggota : () => {
        return api.get(`/anggota/all`)
    },
    anggotaByKey : (key) => {
        return api.get(`/anggota/all/${key}`)
    },
    tambahAnggota : (data) => {
        return api.post(`/anggota/add`,data)
    },
    deleteAnggota : (uuid) => {
        return api.delete(`/anggota/delete/${uuid}`)
    },
    updateAnggota : (uuid,data) => {
        return api.put(`/anggota/update/${uuid}`,data)
    }
    
}
export default Admin