import axios from "axios";
import {store} from "../store"
import { userActions } from "../store/slice/user";

const api = axios.create({
    // baseURL:"http://192.168.1.29:8020"
    baseURL:"https://api-ruangrenung.sandikala.id"
})

api.interceptors.request.use((req) => {
    if (!req.url.includes("/login") || !req.url.includes("/register")) {
      req.headers = {
        Authorization: `Bearer ${store.getState().user.aksesToken}`,
      };
    }
    return req;
  });
  
  api.interceptors.response.use(
    (res) => {
      return res;
    },
    (err) => {
      if (err.response.status === 401) {
        store.dispatch(userActions.logout());
      }
      return Promise.reject(err);
    }
  );
  


export default api