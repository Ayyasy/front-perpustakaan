import React from 'react'
import AsyncSelect from "react-select/async";
import { Controller } from "react-hook-form"


function AsyncOption(props) {
    return (
        <div className="w-full space-y-1 mt-4">
            <label className="md:mx-0 text-md font-light">{props.label}</label>
            <Controller
                control={props.method.control}
                name={props.methodName}
                render={({
                    field: { onChange, onBlur, value, name, ref },
                    fieldState: { invalid, isTouched, isDirty, error },
                    formState,
                }) => (
                    <AsyncSelect
                        className={`basic-multi-select ${props.className}`}
                        classNamePrefix="select"
                        placeholder={props.placeholder}
                        onChange={(e) => {
                            if(props.isMulti){
                                onChange(e);
                                if (props.onChange) {
                                    props.onChange(e);
                                }
                            }
                            else{
                                onChange(e.value);
                                if (props.onChange) {
                                    props.onChange(e.value);
                                }
                            }   
                        }}
                        // ref={ref}
                        // value={props.loadOptions.find((o) => o.value === value)}
                        value={props.loadOptions.target}
                        cacheOptions
                        defaultOptions
                        loadOptions={props.loadOptions}
                    />
                )}
            />
            {props.method.formState.errors[props.methodName] && (
                <p className="text-red-600 mt-1 mx-1">
                    {props.method.formState.errors[props.methodName].message}
                </p>
            )}
        </div>
    );
}

export default AsyncOption