import React from 'react'
import {BiLoader} from "react-icons/bi"

function Loading(props) {
  return (
    <div className={`bg-black bg-opacity-70 w-full h-[100vh] flex justify-center fixed top-0 left-0 z-10 ${props.show ? "visible" : "invisible"}`}>
        <div className=' flex flex-col justify-center'>
            <BiLoader className='w-24 h-24 animate-spin fill-slate-200 '/>
        </div>
    </div>
  )
}

export default Loading