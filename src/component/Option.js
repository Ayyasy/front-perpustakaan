import React from 'react'
import Select from "react-select";
import { Controller } from "react-hook-form"


function Option(props) {
    return (
        <div className="w-full space-y-1 mt-4">
            <label className="md:mx-0 text-md font-light">{props.label}</label>
            <Controller
                control={props.method.control}
                name={props.methodName}
                defaultValue={props.defaultValue}
                render={({
                    field: { onChange, onBlur, value, name, ref },
                    fieldState: { invalid, isTouched, isDirty, error },
                    formState,
                }) => {
                    return <Select
                        className={`basic-multi-select ${props.className}`}
                        classNamePrefix="select"
                        // className='mx-4 md:mx-0'
                        placeholder={props.placeholder}
                        options={props.options}
                        isMulti={props.isMulti}
                        onBlur={props.onBlur}
                        onChange={(e) => {
                            // console.log(e)
                            if (props.isMulti) {
                                onChange(e);
                                if (props.onChange) {
                                    props.onChange(e);
                                }
                            }
                            else {
                                onChange(e.value);
                                if (props.onChange) {
                                    props.onChange(e);
                                }
                            }
                        }}
                        ref={ref}
                        value={props.onChange ? props.value : props.options.find((o) => o.value === value)}
                    />
                }}
            />
            {props.method.formState.errors[props.methodName] && (
                <p className="text-red-600 mt-1 mx-1">
                    {props.method.formState.errors[props.methodName].message}
                </p>
            )}
        </div>
    );
}

export default Option