import React from 'react'

function Text(props) {
  return (
    <div className={props.className}>
        {props.text}
    </div>
  )
}

export default Text