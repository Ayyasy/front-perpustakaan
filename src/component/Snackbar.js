import React, { useEffect, useState } from 'react'
import { ImCross } from "react-icons/im"
import Text from './Text'

function Snackbar(props) {
  const [show, setShow] = useState(props.show)
  useEffect(() => {
    setShow(props.show)
  }, [props.show])
  return (
    <div className={`bg-black bg-opacity-60 w-full h-full fixed top-0 left-0 z-10 ${show ? "visible" : "invisible"}`}>
      <div className={`bg-slate-300 md:w-[40vw] relative w-[95vw] transition-all ease-in-out duration-500 mx-auto ${show ? "mt-7" : "-mt-40"}`}>
        <div className='flex absolute right-0 justify-end p-1 shadow-xl cursor-pointer'>{<ImCross className='bg-slate-100 w-8 h-8 p-1' onClick={() => props.toggle(false)} />}</div>
        <div className=' mx-10'>
          <Text className="text-center font-light md:text-xl py-7" text={props.msg} />
        </div>
      </div>
    </div>
  )
}

export default Snackbar