import React, { useEffect, useState } from 'react'
import {MdOutlineTransitEnterexit} from "react-icons/md"

function Dialog(props) {
    const [show,setShow] = useState(props.show)
    useEffect(() => {
        setShow(props.show)
    },[props.show])
  return (
    <div className={`bg-black fixed top-0 left-0 z-50 w-full bg-opacity-60  h-[100vh] flex justify-center transition-all ease-in-out duration-300 ${show ? "visible" : "invisible"}`}>
        <div className='flex justify-center flex-col '>
            <div className={`bg-slate-100 md:w-[40vw] w-[93vw] shadow-2xl relative ${show ? "mt-0" : "md:-mt-[170vw] -mt-[400vw]"} transition-all ease-in-out duration-1000`}>
                <div className={`bg-slate-200 absolute -right-2 -top-2 shadow-xl cursor-pointer hover:right-0 hover:top-0 transition-all ease-in-out duration-300 ${props.classExit}`} onClick={() => props.toggle(false)}><MdOutlineTransitEnterexit className='w-10 h-10'/></div>
                <div className={`${props.className}`}>
                    {props.children}
                </div>
            </div>
        </div>
    </div>
  )
}

export default Dialog