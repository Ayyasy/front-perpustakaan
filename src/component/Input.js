import React from 'react'

function Input(props) {
    return (
        <div className=''>
            <label className='font-light md:text-[2.4vmin] flex'>{props.tag}{props.name}</label>
            <div className='flex mt-2'>
                <input type={props.type} {...props.method.register(props.methodName)} placeholder={props.placeholder} className={`font-light border  outline-none rounded-md  shadow-xl ${props.method.formState.errors[props.methodName] ? "border-red-600" : "border-blue-900"} ${props.className}`} readOnly={props.readOnly} />
                {props.icon}
            </div>
            {props.method.formState.errors[props.methodName] && <p className='text-red-600 mt-1 mx-1 text-xs'>{props.method.formState.errors[props.methodName].message}</p>}
        </div>

    )
}

export default Input