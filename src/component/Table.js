import React,{useEffect, useState} from 'react'
import Select from 'react-select'

function Table(props) {
    // const [key,setKey] = useState(props.key)
    // useEffect(() => {
    //     setKey(props.key)
    // },[props.key])
    const [row,setRow] = useState(props.row)
    useEffect(()=>{
        setRow(props.row)
    },[props.row])
    return (
        <div className='overflow-x-hidden'>
            <div className='bg-white px-5 pt-3 pb-5 shadow-lg'>
                <h1 className='text-2xl font-serif'>{props.title}</h1>
                <div className='md:flex mt-3 w-full'>
                    <div>
                        <h3 className='mb-1 text-sm font-light'>{props.searchTitle}</h3>
                        <input value={props.value} onChange={props.onChange} className='border-b border-b-[#123456] outline-none px-2 py-1' />
                    </div>
                    <button className='bg-[#123456] text-white hover:bg-[#20558a] py-1 md:ml-2 px-5 rounded-md md:mt-6 mt-2' onClick={props.getByKey}>Cari</button>
                </div>
            </div>
            <div className='mt-7 shadow-xl mb-10 bg-white'>
                <div className='flex justify-between pl-2 pr-5'>
                    <h1 className='text-xl  px-4 py-5 font-extrabold uppercase'>{props.titleTable}</h1>
                    <button className={`bg-gradient-to-tr text-white bg-[#123456] hover:bg-[#1e5083] px-5 py-2 rounded-md my-auto font-light ${props.classTambah}`} onClick={props.dialogTambah}>{props.titleAdd}</button>
                </div>
                <div className='overflow-x-auto w-full'>
                    {props.children}
                    <div className='my-3 flex flex-col px-5'>
                        <div className='flex md:justify-end font-light mt-2 text-xl md:w-full w-96'>
                            <h1 className='my-auto w-24 px-3'>row :</h1>
                            <select className='w-12 outline-none border-b border-blue-700 bg-transparent font-light' id='row' onChange={props.changeRow} >
                                <option value={5} className='font-light'>5</option>
                                <option value={10} className='font-light'>10</option>
                                <option value={15} className='font-light'>15</option>
                                <option value={"all"} className='font-light'>All</option>
                            </select>
                            <div className='my-auto px-3 '>Halaman ke {props.textPage} dari {props.totalPage}</div>
                        </div>
                        <div className='flex md:justify-end space-x-3 md:mx-2 my-3 '>
                            <button className='font-light from-red-600 to-red-500 py-1 px-4 text-white bg-gradient-to-tl rounded-lg hover:to-red-900' onClick={props.prevHandle}>Previous</button>
                            <button className='font-light from-blue-600 to-blue-500 py-1 px-4 text-white bg-gradient-to-tl rounded-lg hover:to-blue-900' onClick={props.nextHandle}>Next</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>)
}

export default Table