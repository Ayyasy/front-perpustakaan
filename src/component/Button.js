import React,{useState,useEffect} from 'react'
import {RiLoaderLine} from "react-icons/ri"

function Button(props) {
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    setLoading(props.loading)
  }, [props.loading])
  return (
    <button className={`rounded-lg ${props.className}`} type={props.type} onClick={props.onClick}>
      {loading ? <RiLoaderLine className='w-6 h-6 mx-auto animate-spin' /> : <div>
        {props.name}
      </div>}
    </button>
  )
}

export default Button